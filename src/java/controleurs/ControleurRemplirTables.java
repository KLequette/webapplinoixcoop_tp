
package controleurs;

import entites.Commande;
import entites.Distributeur;
import entites.Produit;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

@Named
public class ControleurRemplirTables {
    
    @PersistenceContext private EntityManager em;
    @Resource private UserTransaction         utx; 
    
    public String executer() throws Exception{ 
        
       
        /////////////  Distributeurs
        
        Distributeur dist=new Distributeur();
        dist.setId("carr15432");
        dist.setNom("CarreClerc");
       
        
        
        ////////////// Produits
        
        Produit p1= new Produit();
        p1.setVariete("Mayette");
        p1.setType("Fraîche entière");
        p1.setCalibre("2");
        
        Produit p2=new Produit();
        p2.setVariete("Parisienne");
        p2.setType("Cerneaux");
        p2.setCalibre("1");

        
        //////////////// Commandes 
        
        Commande cmd1=  new Commande();
        cmd1.setId(213L);
        cmd1.setQuantité(20);
        cmd1.setPrixHT(6f);
        cmd1.setConditionnement("Filet 1Kg");
        cmd1.setDateConditionnement(utilitaires.UtilDate.parse("08/05/2015"));
                
         
        Commande cmd4=  new Commande();
        cmd4.setId(215L);
        cmd4.setQuantité(50);
        cmd4.setPrixHT(5f);
        cmd4.setConditionnement("Filet 5Kg");
        cmd4.setDateConditionnement(utilitaires.UtilDate.parse("12/05/2015"));
        
 
        
        utx.begin();
        
        
           em.persist(dist);
           em.persist(p1);  em.persist(p2);
           em.persist(cmd1);em.persist(cmd4);
        
        utx.commit();
      
        return "Tables  remplies";
    }
           
}
