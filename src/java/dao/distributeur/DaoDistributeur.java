
package dao.distributeur;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import entites.Distributeur;

@Stateless
public class DaoDistributeur {
    
    @PersistenceContext private EntityManager em;
    public Distributeur getLeDistributeur(String idDist){
   
   return em.find(Distributeur.class, idDist);
   
   
    }
}
