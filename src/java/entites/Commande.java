
package entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import utilitaires.xml.adaptateur.DateAdaptateur;


@Entity
public class Commande implements Serializable {
    
    @Id
    private Long id;
    
     @XmlJavaTypeAdapter(DateAdaptateur.class)
     @Temporal(javax.persistence.TemporalType.DATE)
     private Date dateEnvoi;


     @XmlJavaTypeAdapter(DateAdaptateur.class)
     @Temporal(javax.persistence.TemporalType.DATE)
     private Date    dateConditionnement;
     private Float prixHT;
     private String conditionnement;
     private int quantité;
     
    
    @ManyToOne
     private Produit leProduit;
     
    //<editor-fold defaultstate="collapsed" desc="get set">
    
         public Date getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public Date getDateConditionnement() {
        return dateConditionnement;
    }

    public void setDateConditionnement(Date dateConditionnement) {
        this.dateConditionnement = dateConditionnement;
    }

    public Float getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(Float prixHT) {
        this.prixHT = prixHT;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }

    public int getQuantité() {
        return quantité;
    }

    public void setQuantité(int quantité) {
        this.quantité = quantité;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
   
    public Produit getLeProduit() {
        return leProduit;
    }

    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }


    //</editor-fold>


}