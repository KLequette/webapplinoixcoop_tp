
package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Produit implements Serializable {
    
    @Id
    private String variete;
    private String type;
    private String calibre;
    
    //<editor-fold defaultstate="collapsed" desc="get set">
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCalibre() {
        return calibre;
    }

    public void setCalibre(String calibre) {
        this.calibre = calibre;
    }
    
    public String getVariete() {
        return variete;
    }
    
    public void setVariete(String variete) {
        this.variete = variete;
    }
    //</editor-fold>
    
}
